import { getConnection, Repository } from 'typeorm';
import Invoice from '../entity/invoice';

const COEF_LESS_MONTH = 0.3;
const COEF_MORE_MONTH = 0.5;
const MONTH = 30 * 24 * 60 * 60 * 1000;

class InvoiceService {
  private invoiceRepo: Repository<Invoice>;

  constructor() {
    this.invoiceRepo = getConnection().getRepository(Invoice);
  }

  async parseCsv(csvData) {
    const rows = csvData.split('\n');
    let errorsCount = 0;
    const linesWithErrors = [];
    const invoices = [];
    const currentDate = Date.now();
    rows.forEach((row, index) => {
      const values = row.split(',');
      if (values.length !== 3) {
        linesWithErrors.push(index + 1);
        errorsCount++;
        return;
      }
      const [id, amount, plainDate] = values;
      try {
        const date = new Date(plainDate);
        const sellPrice = InvoiceService.calculateNewAmount(
          parseFloat(amount),
          date,
          currentDate,
        );
        invoices.push(new Invoice({ id, date, sellPrice }));
      } catch (e) {
        errorsCount++;
        linesWithErrors.push(index + 1);
      }
    });
    await this.invoiceRepo.insert(invoices);
    return { success: invoices.length, errors: errorsCount, linesWithErrors };
  }

  getInvoices(pageNum, pagination) {
    return this.invoiceRepo.find(
      InvoiceService.calculatePagination(pageNum, pagination),
    );
  }

  async totalInvoices() {
    const { count } = await this.invoiceRepo.createQueryBuilder('invoice')
      .select('COUNT(*)', 'count')
      .getRawOne();
    return count;
  }

  private static calculatePagination(pageNum, perPage) {
    if (pageNum <= 0 || !pageNum) {
      return {
        skip: 0,
        take: perPage,
      };
    }
    const skip = (pageNum - 1) * perPage;
    const take = skip + perPage;
    return {
      skip,
      take,
    };
  }

  private static calculateNewAmount(amount, invoiceDate, currentDate) {
    const coef = (currentDate - invoiceDate) > MONTH ? COEF_MORE_MONTH : COEF_LESS_MONTH;
    return amount * coef;
  }
}

export default InvoiceService;
