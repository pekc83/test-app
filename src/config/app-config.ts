import ormConfig from './orm-config';

const appConfig = {
  port: process.env.PORT,
  dbConnection: ormConfig,
};

export default appConfig;
