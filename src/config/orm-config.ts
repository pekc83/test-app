import { ConnectionOptions } from 'typeorm';

const ormConfig: ConnectionOptions = {
  type: 'mysql',
  host: process.env.MYSQL_HOST,
  port: Number(process.env.MYSQL_PORT),
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  logging: false,
  synchronize: false,
  entities: [
    'dist/entity/*.js',
  ],
  extra: {
    ssl: {
      rejectUnauthorized: false,
    },
  },
};

export default ormConfig;
