import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
class Invoice {
  @PrimaryColumn()
  public id: string;

  @Column()
  public date: Date;

  @Column({ type: 'float' })
  public sellPrice: number;

  constructor(invoice?: Partial<Invoice>) {
    this.id = invoice?.id;
    this.date = invoice?.date;
    this.sellPrice = invoice?.sellPrice;
  }
}

export default Invoice;
