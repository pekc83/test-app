import express from 'express';
import InvoiceController from '../controllers/invoice-controller';

const initRouter = () => {
  const router = express.Router();
  const invoiceController = new InvoiceController();

  router.post('/upload-csv', invoiceController.uploadCsv.bind(invoiceController));

  router.get('/', invoiceController.getInvoices.bind(invoiceController));
  return router;
};

export default initRouter;
