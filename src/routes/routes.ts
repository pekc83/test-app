import express from 'express';
import initInvoiceRouter from './invoice-router';

const initRouter = () => {
  const router = express.Router();
  router.use('/invoice', initInvoiceRouter());
  return router;
};

export default initRouter;
