import { createConnection } from 'typeorm';
import 'reflect-metadata';
import initApp from './app';
import appConfig from './config/app-config';

const PORT = appConfig.port || 7777;

createConnection(appConfig.dbConnection)
  .then(() => {
    console.log('Connect DB success');
    initApp().listen(PORT, () => { console.log(`server start listen on ${PORT}`); });
  })
  .catch((err) => {
    console.log(err);
  });
