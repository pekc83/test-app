import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateInvoice1591030723233 implements MigrationInterface {
    name = 'CreateInvoice1591030723233'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `invoice` (`id` varchar(255) NOT NULL, `date` datetime NOT NULL, `sellPrice` float NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `invoice`", undefined);
    }

}
