import { Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';
import InvoiceService from '../services/invoice-service';

class InvoiceController {
  private invoiceService: InvoiceService;

  constructor() {
    this.invoiceService = new InvoiceService();
  }

  async uploadCsv(req: Request, res: Response) {
    const csvFile = req.files.upload as UploadedFile;
    try {
      const result = await this.invoiceService.parseCsv(csvFile.data.toString('utf-8'));
      res.status(200).json({
        success: result.success,
        errors: result.errors,
        lines: result.linesWithErrors,
      });
    } catch (e) {
      res.status(500).json(e);
    }
  }

  async getInvoices(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const invoices = await this.invoiceService.getInvoices(page, pageSize);
    const total = await this.invoiceService.totalInvoices();
    res.status(200).json({ data: invoices, total });
  }
}


export default InvoiceController;
