import {Component, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material';
import { UploadService } from './shared/services/upload.service';

export interface Invoice {
  id: string;
  sellPrice: number;
  date: string;
}

interface UploadResponse {
  success: number;
  error: number;
  index: number[];
}

type Tab = 'upload' | 'invoices';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  fileToUpload: File = null;

  currentTab: Tab = 'upload';

  errorMessage = 'Upload error, please try again';

  isError = false;

  uploadResponse: UploadResponse;

  constructor(private uploadService: UploadService) {}

  uploadFile() {
    this.uploadService.uploadFile(this.fileToUpload).subscribe((res) => {
      this.uploadResponse = res;
    }, () => {
      this.isError = true;
    });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  toggleBar(tab: Tab) {
    this.currentTab = tab;
  }
}
