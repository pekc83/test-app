import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  uploadEndpoint = environment.apiRoot + 'invoice/upload-csv';
  invoiceEndpoint = environment.apiRoot + 'invoice/';

  constructor(private httpClient: HttpClient) { }

  uploadFile(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('upload', fileToUpload, fileToUpload.name);
    return this.httpClient.post(this.uploadEndpoint, formData);
  }

  getInvoices(page: number, pageSize: number): Observable<any> {
    page += 1;
    const params = new HttpParams()
      .set('page', `${page}`)
      .set('pageSize', `${pageSize}`);
    return this.httpClient.get<any>(this.invoiceEndpoint, { params });
  }
}
