import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material';
import {
  catchError, map, startWith, switchMap,
} from 'rxjs/operators';
import {of, Subscription} from 'rxjs';
import {Invoice} from '../app.component';
import {UploadService} from '../shared/services/upload.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  data: Invoice[] = [];
  displayedColumns = ['id', 'sellPrice', 'date'];
  pageSize = 10;
  totalLength = 0;
  isLoadingResults = false;

  private sub: Subscription;

  constructor(private uploadService: UploadService) {}

  ngOnInit(): void {
    this.sub = this.paginator.page.pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.uploadService.getInvoices(
          this.paginator.pageIndex,
          this.paginator.pageSize || this.pageSize,
        );
      }),
      map((res) => {
        this.isLoadingResults = false;
        this.totalLength = res.total;
        return res.data;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        return of([]);
      }),
    ).subscribe((data: Invoice[]) => this.data = data);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
